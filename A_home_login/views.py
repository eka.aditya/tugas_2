from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
# Create your views here.
response = {'logged_in' : False}

def index(request):
    html = 'A_home_login/session/home.html'
    return render(request, html, response)

def login(request):
    print ("#==> masuk index")
    if 'user_login' in request.session:
        return HttpResponseRedirect(reverse('mahasiswa:index'))
    else:
        html = 'A_home_login/session/login.html'
        return render(request, html, response)
